# docker-au-eoed-dev
A Docker image packaging the various software from the Aberystwyth University (AU) Earth Observation and Ecosystem Dynamics (EOED) research group, primarily developed by Pete Bunting. More information can be found at https://www.remotesensing.info.

AU-EOED-DEV software includes:

* RSGISLib (https://www.rsgislib.org)
* ARCSI (https://arcsi.remotesensing.info)
* EODataDown (https://eodatadown.remotesensing.info)
* compute job recorder (https://bitbucket.org/petebunting/compute_job_recorder)
* pb slurm user tools (https://bitbucket.org/petebunting/pb_slurm_user_tools)

Other software:

* GDAL
* scikit-learn
* scikit-image
* matplotlib
* numpy
* pandas
* scipy
* statsmodels
* rios
* postgresql
* sqlachemly
* requests
* planet
* pycurl
* psycopg2
* google-cloud-python
* pyyaml
* jinja2

This image is based on the official ubuntu release with Python 3.6 and packages from ContinuumIO Miniconda3.

## Build image

docker build -t petebunting/au-eoed-dev https://petebunting@bitbucket.org/petebunting/docker-au-eoed-dev.git

## Pulling image

docker pull petebunting/au-eoed-dev

## Running Commands

To run commands, you need to mount the data path of where the data is located and where they are
mounted within the image. In this example, /home/pete/temp is mounted as /data and therefore the
data paths for the data processing need to be referenced from that path e.g., /data/TestImg.kea.

docker run -i -t -v /home/pete/temp:/data  petebunting/au-eoed-dev gdalinfo -norat /data/TestImg.kea

For tutorials in using rsgislib, arcsi and gdal there are some materials available at 
https://www.remotesensing.info.

